# TTA GitPod VS Code

## Getting started

1. gitlab 가입

2. [TTA-CHAMP / Gitpod · GitLab](https://gitlab.com/tta-champ/gitpod) fork 주소
image.png
![fork.png](./fork.png)

3. gitlab 계정으로 gitpod.io 가입

![gitpod.png](./gitpod.png)

4. start project

It takes 30minutes

## Start myself

Click the button below to start a new development environment:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://github.com/gitpod-io/template-k3s)


## Notes & caveats

Here's a diagram of the interactions that also shows how the various components interact with each other.

<center>

![diagram.svg](diagram.svg)

</center>

## Usage

At start, the workspace will start a VM in your gitpod workspace and
automatically install k3s on it. Your local environment will be automatically
configured to use that via the `kubectl`


### Connecting via kubectl

When you open your workspace terminal, the `kubectl` is already configured for you
via the `~/.kube/config` file.

### Kubectl from my local terminal


### Connecting via SSH

You can connect to the VM via ssh at any moment. The ssh daemon
is exposed on `127.0.0.1` for the workspace on port `2222`.

- username: root
- password: root

```console
ssh -p 2222 root@127.0.0.1
```

You can use the `.gitpod/ssh.sh` and `.gitpod/scp.sh` scripts if you want to
avoid the extra steps.
