# 3. VSCode for IDE

Date: 2023-02-13

## Status

Superseded by [4. intellij is IDE](0004-intellij-is-ide.md)

## Context

IDEs
 - vscode : free, 
 - intelli J : commercial
 - eclipse : free

vscode is most popular ide.


## Decision

We will use VSCode IDE

## Consequences

QA team should make coding convection to vs setting file.
